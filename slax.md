Memasang Distro Slax (Debian Based) ke HDD.

Slax merupakan salah satu distro yang terkenal ringan dan bisa dijalankan secara portabel. Slax biasanya dipasang pada perangkat USB flashdisk dan dijalanakan dengan opsi `persistent`. Bagaimana jika kita ingin memasang Slax di HDD seperti distro yang umum ? Ternyata bisa, lho ? Berikut adalah pembahasannya.

## Persiapan.

Berikut adalah beberapa hal yang perlu kamu persiapkan :

1. Satu partisi untuk root (/)
1. Satu partisi untuk swap (opsional).

NB: Kali ini, kita menggunakan bios legacy dan MBR.

## Langkah-langkah.

Berikut adalah langkah-langkah yang bisa kamu jalankan :

1. **Boot ISO slax.**


1. **Buka terminal (xterm).**

    Setelah masuk tampilan GUI slax, bukalah terminal xterm !


1. **Format partisi.**

    Kita asumsikan kamu sudah menyiapkan satu partisi khusus, kamu bisa memformatnya ke `ext4`. Perintahnya adalah sebagai berikut :

    > \# mkfs.ext4 /dev/abc1


1. **Mount partisi.**

    Mount partisi yang sudah diformat ke direktori `/mnt` atau lokasi yang diinginkan ! Perintahnya adalah sebagai berikut :

    > \# mount /dev/abc1 /mnt


1.  **Menyalin isi root (/) ke partisi yang di-mount sebelumnya.**

    Salinlah semua isi root (/) ke /mnt ! Perintahnya adalah sebagai berikut :

    > \# cp -ax / /mnt


1. **Chroot !**

    Masuklah ke direktori /mnt lalu mount beberapa filesystem dilanjutkan chroot !

    > \# cd /mnt

    > \# mount -t proc proc proc/

    > \# mount --rbind /sys sys/

    > \# mount --rbind /dev dev/

    > \# mount --rbind /run run/

    > \# chroot /mnt /bin/bash


1. **Edit fstab !** 

    Editlah berkas `fstab` sesuai kebutuhan ! Kamu bisa menggunakan editor `mcedit` yang tersedia.

    > \# cp /proc/mounts /etc/fstab

    > \# mcedit /etc/fstab 

    Contoh sederhana baris fstab :

    ```
    UUID=/dev/abc1       /       ext4    errors=remount-ro       0       1
    ```


1. **Konfigurasi source apt.**

    Edit (opsional) berkas /etc/apt/sources.list, bisa juga menggunakan bawaan slax.

    > \# mcedit /etc/apt/sources.list

    Setelah diedit, alangkah baiknya kita melakukan update dan upgrade.

    > \# apt update && apt upgrade


1. **Memilih zona waktu.**

    Kita perlu memilih zona waktu sesuai lokasi kita. Kita bisa mengaturnya lewat perintah berikut :

    > \# dpkg-reconfigure tzdata


1. **Konfigurasi locales.**

    Kita perlu mengatur locales. Kita bisa mengaturnya lewat perintah berikut :

    > \# dpkg-reconfigure locales


1. **Mengatur hostname.**

    Hostname bisa kita atur dengan mengedit berkas `/etc/hostname`, Kita bisa mengaturnya lewat perintah berikut :

    > \# echo "hostname_anda" > /etc/hostname


1. **Mengatur berkas hosts.** 

    Berkas hosts bisa kita atur dengan mengedit berkas `/etc/hosts`, Kita bisa mengaturnya lewat perintah berikut :

    > \# mcedit /etc/hosts
    
    Sesuaikan dengan kebutuhan, berikut adalah contohnya : 

    ```
    127.0.0.1	LIDG
    ::1		LIDG

    127.0.0.1	LIDG slax
    ::1		LIDG slax

    ```


1. **Memasang paket grub.**

    Kita perlu memasang paket grub agar instalasi kita bisa boot. Kita bisa memasangnya lewat perintah berikut :

    > \# apt install grub2


1. **Mengatur passwd root ,membuat user dan passwordnya.**

    Kita perlu mengatur ulang password untuk root, lalu membuat user biasa dan password user. Kita bisa mengaturnya lewat perintah berikut :

    > \# passwd

    > \# useradd USERNAME -m

    > \# passwd USERNAME


1. **Memasang bootloader pada disk.**

    Agar bisa boot dengan lancar, kita perlu memasang bootloader pada disk kita. Kita bisa memasangnya lewat perintah berikut :

    > \# grub-install /dev/abc

    Jangan lupa mengupdate grub juga !

    > \# update-grub 


1. **Keluar chroot.**

    Kita bisa keluar dari chroot dengan perintah berikut :

    > \# exit 

    atau bisa dengan menekan kombinasi Ctrl+d.


1. **Reboot.**

    Setelah dirasa cukup, kita bisa langsung _reboot_ / dinyalakan ulang. Perintahnya adalah sebagai berikut :

    > \# systemctl reboot
    

### Tambahan :

1. Bila mengalami tidak bisa memulai "X" dengan user biasa / non-root, coba install paket xserver-xorg-legacy. Buat satu file konfigurasi di /etc/X11/Xwrapper.config.

    > \# vim /etc/X11/Xwrapper.config

    isinya adalah sebagai berikut :

    ```
    allowed_users=console
    needs_root_rights=yes
    ```


1. Disable root autologin bawaan slax.

    > \# systemctl set-default multi-user.target


1. Asumsi koneksi internet dari ethernet atau kabel LAN.

    > \# dhclient ens3 -v

